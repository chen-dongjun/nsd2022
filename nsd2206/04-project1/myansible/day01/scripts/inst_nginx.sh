#!/bin/bash

cd /root
tar xf nginx-1.12.2.tar.gz
cd nginx-1.12.2/
./configure --with-http_ssl_module --with-http_stub_status_module
make && make install
