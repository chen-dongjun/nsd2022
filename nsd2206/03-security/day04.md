# day04

[toc]

## kali

- 实际上它就是一个预安装了很多安全工具的Debian Linux

```shell
[root@zzgrhel8 ~]# kali reset
kali reset OK.
该虚拟机系统用户名为:kali,密码为:kali
```

- 基础配置

```shell
$ ip a s    # 查看网络
$ nmcli connection show    # 查看到网络连名为"Wired connection 1"
$ nmcli connection modify "Wired connection 1" ipv4.method manual ipv4.address 192.168.88.40/24 autoconnect yes
$ nmcli connection up "Wired connection 1"
$ systemctl start ssh      # 启ssh服务，弹出的窗口输入密码kali
[root@zzgrhel8 ~]# ssh kali@192.168.88.40
kali@192.168.88.40's password: kali
┌──(kali㉿kali)-[~]
└─$ 
```

### nmap扫描

- 一般来说扫描是攻击的前奏。
- 扫描可以识别目标对象是什么系统，开放了哪些服务。
- 获知具体的服务软件及其版本号，可以使得攻击的成功率大大提升。
- 扫描可以检测潜在的风险，也可以寻找攻击目标、收集信息、找到漏洞
- windows下，扫描可以使用xscan / superscan
- Linux，扫描可以采用nmap
- 吾爱破解：https://www.52pojie.cn/
- 中国黑客团队论坛：https://www.cnhackteam.org/
- 俄罗斯论坛：https://rutracker.org/

```shell
┌──(kali㉿kali)-[~]
└─$ nmap
# -sT: TCP扫描。
# -U: 扫描目标的UDP端口。
# -sP：ping扫描
# -A：对目标系统全面分析

# 扫描整个网段，哪机器可以ping通
┌──(kali㉿kali)-[~]
└─$ nmap -sP 192.168.88.0/24

# 扫描192.168.88.11开放了哪些TCP端口
┌──(kali㉿kali)-[~]
└─$ sudo nmap -sT 192.168.88.11

# 扫描192.168.88.11开放了哪些UDP端口。非常耗时！
┌──(kali㉿kali)-[~]
└─$ sudo nmap -sU 192.168.88.11

# 全面扫描192.168.88.11系统信息
┌──(kali㉿kali)-[~]
└─$ sudo nmap -A 192.168.88.11 
```

- 使用脚本扫描

```shell
# 通过脚本扫描目标主机的ftp服务
# 在目标主机上安装vsftpd服务
[root@node1 ~]# yum install -y vsftpd
[root@node1 ~]# systemctl start vsftpd

# 在kali主机上查看有哪些脚本
┌──(kali㉿kali)-[~]
└─$ ls /usr/share/nmap/scripts/

# 扫描ftp服务是否支持匿名访问。ftp控制连接端口号是21
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-anon.nse 192.168.88.11 -p 21
21/tcp open  ftp
| ftp-anon: Anonymous FTP login allowed   # 允许匿名访问

# 扫描ftp相关信息，如版本号、带宽限制等
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-syst.nse 192.168.88.11 -p 21

# 扫描ftp后门漏洞
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ftp-vsftpd-backdoor 192.168.88.11 -p 21
```

- 扫描口令

```shell
# 通过ssh协议，使用nmap自带的密码本扫描远程主机的用户名和密码
# 在目标主机上创建名为admin的用户，密码为123456
[root@node1 ~]# useradd admin
[root@node1 ~]# echo 123456 | passwd --stdin admin

# 在kali上扫描弱密码
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ssh-brute.nse 192.168.88.11 -p 22

# 通过ssh协议，使用nmap以及自己的密码本扫描远程主机的密码
# 1. 创建用户名文件
┌──(kali㉿kali)-[~]
└─$ sudo echo root > /tmp/users.txt
                                                        
┌──(kali㉿kali)-[~]
└─$ cat /tmp/users.txt 
root

[root@node1 ~]# echo 19910101 | passwd --stdin root
# 2. 生成1990-01-01到2020-12-31之间的所月日期
# 在centos7上，创建以下文件
[root@cent7 ~]# vim mydate.py
from datetime import datetime, timedelta

d1 = datetime(1989, 12, 31)
d2 = datetime(2021, 1, 1)
dt = timedelta(days=1)

with open('/tmp/mima.txt', 'w') as f:
    while d1 < d2:
        d1 += dt
        f.write("%s\n" % d1.strftime('%Y%m%d'))
[root@cent7 ~]# python mydate.py
[root@cent7 ~]# scp /tmp/mima.txt kali@192.168.88.40:/tmp/

# 3. 使用自己的密码本破解密码
┌──(kali㉿kali)-[~]
└─$ sudo nmap --script=ssh-brute.nse --script-args userdb=/tmp/users.txt,passdb=/tmp/mima.txt 192.168.88.11 -p 22

# 4. 目标主机将会记录所有的登陆事件
[root@node1 ~]# vim /var/log/secure
# 查看最近的登陆失败事件
[root@node1 ~]# lastb
# 查看最近的登陆成功事件
[root@node1 ~]# last
```

- 扫描windows口令

```shell
[root@zzgrhel8 ~]# cat /tmp/winuser.txt    # windows用户名
administrator
admin
# 通过samba服务扫描密码
[root@zzgrhel8 ~]# nmap --script=smb-brute.nse --script-args userdb=/tmp/winuser.txt,passdb=/tmp/mima 172.40.0.151
```

## 使用john破解密码

- 在线破解哈希值的网站：https://cmd5.com/
- 哈希是算法，英文hash的音译，包括md5、sha等
  - 哈希算法是一种单向加密的算法，也就是将原始数据生成一串“乱码”
  - 只能通过原始数据，生成这串“乱码”，但是不能通过“乱码”回推出原始数据
  - 相同的原始数据，生成的乱码是相同的。
- kali系统提供了一个名为john的工具，可用于密码破解

```shell
[root@node1 ~]# echo 123456 | passwd --stdin root
[root@node1 ~]# useradd tom
[root@node1 ~]# echo abc123 | passwd --stdin tom
[root@node1 ~]# useradd jerry
[root@node1 ~]# echo 123123 | passwd --stdin jerry
[root@node1 ~]# scp /etc/shadow kali@192.168.88.40:/home/kali/

# 字典暴力破解，密码本是/usr/share/john/password.lst
┌──(kali㉿kali)-[~]
└─$ sudo john shadow  

# 直接显示破解的密码，不显示其他额外信息
┌──(kali㉿kali)-[~]
└─$ sudo john --show shadow                
root:123456:18912:0:99999:7:::
tom:abc123:18912:0:99999:7:::
jerry:123123:18912:0:99999:7:::


# 字典暴力破解，指定密码本文件
┌──(kali㉿kali)-[~]
└─$ sudo john --wordlist=/tmp/mima.txt shadow
```

## 抓包

- 传输的各种数据，在网络中都是一个个的数据包

```shell
┌──(kali㉿kali)-[~]
└─$ sudo tcpdump
# -i：指定抓取哪块网卡进入的数据包
# -A：转换为ASCII码，使得可读
# -w：抓包写入文件
# -r：从文件中读取抓包信息
# 抓包时可以过滤要抓哪些包
# 使用host过滤主机，使用net过滤网段，使用port过滤端口... ...

# 1. 抓包：抓取eth0上进出的、与192.168.88.11有关的、涉及TCP21端口的数据包。以下命令执行后，打开新终端。
┌──(kali㉿kali)-[~]
└─$ sudo tcpdump -i eth0 -A host 192.168.88.11 and tcp port 21

# 2. 在新终端登陆ftp
┌──(kali㉿kali)-[~]
└─$ ftp 192.168.88.11
Connected to 192.168.88.11.
220 (vsFTPd 3.0.2)
Name (192.168.88.11:kali): tom   # 用户名
331 Please specify the password.
Password:abc123   # 此处是tom的密码
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> exit    # 退出
221 Goodbye.

# 3.在tcpdump终端可以看到明文的用户名和密码



# 1. 将抓到的包存入文件ftp.cap
┌──(kali㉿kali)-[~]
└─$ sudo tcpdump -i eth0 -A -w ftp.cap host 192.168.88.11 and tcp port 21
# 2. 在另一个终端访问ftp
# 在新终端登陆ftp
┌──(kali㉿kali)-[~]
└─$ ftp 192.168.88.11
Connected to 192.168.88.11.
220 (vsFTPd 3.0.2)
Name (192.168.88.11:kali): tom   # 用户名
331 Please specify the password.
Password:abc123   # 此处是tom的密码
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> exit    # 退出
221 Goodbye.

# 3. 在抓包终端ctrl+c停止
# 4. 读取抓到的包，并过滤
┌──(kali㉿kali)-[~]
└─$ tcpdump -A -r ftp.cap | egrep 'USER|PASS' 
```

- 图形工具：wireshark

```shell
[root@zzgrhel8 ~]# yum install wireshark ftp
```

选择抓哪块网卡进出的数据，然后点左上角的开始

![image-20211012165723032](../imgs/image-20211012165723032.png)

抓到包后，点击左上角同样位置停止，查看数据

![image-20211012165852292](../imgs/image-20211012165852292.png)

## 安全加固

### nginx安全

- 安装启服务

```shell
[root@node1 lnmp_soft]# yum install -y gcc pcre-devel zlib-devel
[root@node1 lnmp_soft]# tar xf nginx-1.12.2.tar.gz 
[root@node1 lnmp_soft]# cd nginx-1.12.2/
[root@node1 nginx-1.12.2]# ./configure && make && make install
[root@node1 ~]# /usr/local/nginx/sbin/nginx 
```

- 访问不存在的路径

![image-20211012172841856](../imgs/image-20211012172841856.png)

- 命令行访问：

```shell
[root@node1 ~]# curl -I http://192.168.88.11/    # -I 只显示头部
HTTP/1.1 200 OK
Server: nginx/1.12.2
Date: Fri, 10 Dec 2021 07:51:08 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Fri, 10 Dec 2021 07:46:16 GMT
Connection: keep-alive
ETag: "61b305c8-264"
Accept-Ranges: bytes
```

- 隐藏版本信息

```shell
[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 17 http {
 18     server_tokens off;
... ...
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload
```

再次访问不存在的路径，版本号消失

![image-20211012173446335](../imgs/image-20211012173446335.png)

- 防止DOS、DDOS攻击
- DDOS：分布式拒绝服务

```shell
# 压力测试，每批次发送100个请求给web服务器，一共发200个
[root@zzgrhel8 ~]# yum install -y httpd-tools
[root@zzgrhel8 ~]# ab -c 100 -n 200 http://192.168.88.11/ 
... ...
Benchmarking 192.168.88.11 (be patient)
Completed 100 requests
Completed 200 requests
Finished 200 requests    # 发送200个请求完成
... ... 
Complete requests:      200   # 完成了200个请求
Failed requests:        0     # 0个失败
... ...
```

- 配置nginx连接共享内存为10M，每秒钟只接收一个请求，最多有5个请求排队，多余的拒绝

```shell
[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
 17 http {
 18     limit_req_zone $binary_remote_addr zone=one:10m rate=1r/s;   # 添加
... ...
 37     server {
 38         listen       80;
 39         server_name  localhost;
 40         limit_req zone=one burst=5;  # 添加
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload

# 再次测试
[root@zzgrhel8 ~]# ab -c 100 -n 200 http://192.168.88.11/ 
... ...
Benchmarking 192.168.88.11 (be patient)
Completed 100 requests
Completed 200 requests
Finished 200 requests
... ...
Complete requests:      200
Failed requests:        194   # 失败了194个
... ...
```



> 附：取出nginx.conf中注释和空行以外的行
>
> ```shell
> # -v是取反。^ *#表示开头有0到多个空格，然后是#。^$表示空行
> [root@node1 ~]# egrep -v '^ *#|^$' /usr/local/nginx/conf/nginx.conf
> ```

### 防止缓冲区溢出

- 缓冲区溢出定义：程序企图在预分配的缓冲区之外写数据。
- 漏洞危害：用于更改程序执行流，控制函数返回值，执行任意代码。

```shell
# 配置nginx缓冲区大小，防止缓冲区溢出
[root@node1 ~]# vim /usr/local/nginx/conf/nginx.conf
... ...
 17 http {
 18     client_body_buffer_size     1k;
 19     client_header_buffer_size   1k;
 20     client_max_body_size        1k;
 21     large_client_header_buffers 2 1k;
... ...
[root@node1 ~]# /usr/local/nginx/sbin/nginx -s reload
```

## Linux加固

- 设置tom账号，有效期为2022-1-1

```shell
# 查看tom的账号信息
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：从不
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

[root@node1 ~]# chage -E 2022-1-1 tom
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：1月 01, 2022
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

# 设置账号永不过期，注意-E后面是数字-1，不是字母l
[root@node1 ~]# chage -E -1 tom
[root@node1 ~]# chage -l tom
最近一次密码修改时间					：10月 12, 2021
密码过期时间					：从不
密码失效时间					：从不
帐户过期时间						：从不
两次改变密码之间相距的最小天数		：0
两次改变密码之间相距的最大天数		：99999
在密码过期之前警告的天数	：7

# 设置新建用户的密码策略
[root@node1 ~]# vim /etc/login.defs 
 25 PASS_MAX_DAYS   99999    # 密码永不过期，设置最长有效期
 26 PASS_MIN_DAYS   0        # 密码最短使用时间，0表示随时可改密码
 27 PASS_MIN_LEN    5        # 密码最短长度
 28 PASS_WARN_AGE   7        # 密码过期前7天发警告
 33 UID_MIN                  1000   # 新建用户最小的UID
 34 UID_MAX                 60000   # 新建用户最大的UID
```

- 用户安全设置

```shell
# 锁定tom账号
[root@node1 ~]# passwd -l tom
锁定用户 tom 的密码 。
passwd: 操作成功

[root@node1 ~]# passwd -S tom   # 查看状态
tom LK 2021-10-12 0 99999 7 -1 (密码已被锁定。)

# 解锁tom账号
[root@node1 ~]# passwd -u tom
解锁用户 tom 的密码。
passwd: 操作成功
[root@node1 ~]# passwd -S tom
tom PS 2021-10-12 0 99999 7 -1 (密码已设置，使用 SHA512 算法。)
```

- 保护文件

```shell
# 查看文件的特殊属性
[root@node1 ~]# lsattr /etc/passwd
---------------- /etc/passwd    # 没有特殊属性

# 修改属性
chattr +i 文件    # 不允许对文件做任何操作，只能看
chattr -i 文件    # 去除i属性
chattr +a 文件    # 文件只允许追加
chattr -a 文件    # 去除a属性

[root@node1 ~]# chattr +i /etc/passwd
[root@node1 ~]# lsattr /etc/passwd
----i----------- /etc/passwd
[root@node1 ~]# useradd zhangsan
useradd：无法打开 /etc/passwd
[root@node1 ~]# rm -f /etc/passwd
rm: 无法删除"/etc/passwd": 不允许的操作
[root@node1 ~]# chattr -i /etc/passwd
[root@node1 ~]# rm -f /etc/passwd   # 可以删除
[root@node1 ~]# ls /etc/passwd
ls: 无法访问/etc/passwd: 没有那个文件或目录

# 恢复passwd文件
[root@node1 ~]# cp /etc/passwd- /etc/passwd

# 追加
[root@node1 ~]# chattr +a /etc/hosts
[root@node1 ~]# echo 'hello world' >> /etc/hosts
[root@node1 ~]# rm -f /etc/hosts
rm: 无法删除"/etc/hosts": 不允许的操作
```

