# day01

[toc]

- 对服务的管理，不能仅限于可用性。
- 还需要服务可以安全、稳定、高效地运行。
- 监控的目的：早发现、早治疗。
- 被监控的资源类型：
  - 公开数据：对外开放的，不需要认证即可获取的数据
  - 私有数据：对外不开放，需要认证、权限才能获得的数据

## 监控命令

```shell
# uptime可以查看系统已经运行了多久。最后的3个数字分别是CPU最近1分钟、5分钟、15分钟的平均负载。平均负载的值，不应该大于总核心数。
[root@zzgrhel8 ~]# uptime 
 09:28:25 up 53 days, 27 min,  0 users,  load average: 0.09, 0.08, 0.03

[root@zzgrhel8 ~]# free -m      # 主要用于查看内存
[root@zzgrhel8 ~]# swapon -s    # 查看swap空间
[root@zzgrhel8 ~]# df -h        # 查看硬盘使用情况
[root@zzgrhel8 ~]# ping -c2 www.baidu.com   # 发2个包，测试网络
```

## zabbix

- 实施监控的几个方面：
  - 数据采集：使用agent（可安装软件的系统上）、SNMP（简单网络管理协议，用于网络设备的数据采集）
  - 数据存储：使用mysql数据库
  - 数据展示：通过web页面

- zabbix通过在远程主机上安装agent进行数据采集，存储到mysql数据库，通过web页面进行展示。

### 安装zabbix

- 中文手册：https://www.zabbix.com/documentation/3.4/zh/manual

| 主机名       | 地址             |
| ------------ | ---------------- |
| zabbixserver | 192.168.4.5/24   |
| web1         | 192.168.4.100/24 |
| web2         | 192.168.4.200/24 |

```shell
# 准备3台主机。按上表配置主机名和IP地址
[root@zzgrhel8 ~]# clone-vm7 
```

- 安装zabbix

```shell
# 1. 关闭SELINUX和防火墙

# 2. 配置yum。千万注意：只保留系统yum，删除mysql和pxc。
[root@zabbixserver ~]# cat /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Base
baseurl=ftp://192.168.4.254/centos-1804
enabled=1
gpgcheck=0

# 3. 配置nginx，用于展示zabbix的web页面
# 3.1 安装nginx的依赖包
[root@zabbixserver ~]# yum install -y gcc pcre-devel openssl-devel
# 3.2 安装nginx
[root@zzgrhel8 ~]# scp /linux-soft/2/lnmp_soft.tar.gz 192.168.4.5:/root/
[root@zabbixserver ~]# tar xf lnmp_soft.tar.gz 
[root@zabbixserver ~]# cd lnmp_soft/
[root@zabbixserver lnmp_soft]# tar xf nginx-1.12.2.tar.gz 
[root@zabbixserver lnmp_soft]# cd nginx-1.12.2/
[root@zabbixserver nginx-1.12.2]# ./configure --with-http_ssl_module    # 配置nginx支持https
[root@zabbixserver nginx-1.12.2]# make && make install

# 4. 配置nginx支持php。配置php可以连接mysql
[root@zabbixserver ~]# yum install -y php php-fpm php-mysql mariadb-server mariadb-devel

# 5. 根据zabbix手册，修改nginx参数
[root@zabbixserver ~]# vim /usr/local/nginx/conf/nginx.conf
 34     fastcgi_buffers 8 16k;     #缓存php生成的页面内容，8个16k
 35     fastcgi_buffer_size 32k;   #缓存php生产的头部信息，32k
 36     fastcgi_connect_timeout 300;  #连接PHP的超时时间，300秒
 37     fastcgi_send_timeout 300;     #发送请求的超时时间，300秒
 38     fastcgi_read_timeout 300;     #读取请求的超时时间，300秒
 70         location ~ \.php$ {
 71             root           html;
 72             fastcgi_pass   127.0.0.1:9000;
 73             fastcgi_index  index.php;
 74         #    fastcgi_param  SCRIPT_FILENAME  /script    s$fastcgi_script_name;
 75             include        fastcgi.conf;  # 注意改成fastcgi.conf
 76         }

# 6. 启动相关服务
[root@zabbixserver ~]# systemctl enable mariadb --now
[root@zabbixserver ~]# systemctl enable php-fpm --now
[root@zabbixserver ~]# ss -tlnp |grep :9000
LISTEN     0      128    127.0.0.1:9000
[root@zabbixserver ~]# /usr/local/nginx/sbin/nginx 
[root@zabbixserver ~]# ss -tlnp | grep :80
LISTEN     0      128          *:80
# 写入到rc.local中的命令，开机时自动执行
[root@zabbixserver ~]# echo '/usr/local/nginx/sbin/nginx' >> /etc/rc.d/rc.local 
[root@zabbixserver ~]# chmod +x /etc/rc.d/rc.local


# 7. 编译安装zabbix
# 7.1 安装zabbix的依赖包
[root@zabbixserver lnmp_soft]# yum install -y net-snmp-devel curl-devel autoconf libevent-devel
# 7.2 编译安装
[root@zabbixserver ~]# cd lnmp_soft/
[root@zabbixserver lnmp_soft]# ls zabbix-3.4.4.tar.gz 
zabbix-3.4.4.tar.gz
[root@zabbixserver lnmp_soft]# tar xf zabbix-3.4.4.tar.gz 
[root@zabbixserver lnmp_soft]# cd zabbix-3.4.4/
[root@zabbixserver zabbix-3.4.4]# ./configure --enable-server --enable-agent --with-mysql=/usr/bin/mysql_config  --with-net-snmp --with-libcurl
# --enable-server: 安装服务器端
# --enable-agent: 安装被控端
# --with-mysql: 指定mysql数据库配置程序
# --with-net-snmp: 配置可以通过snmp收集数据
# --with-libcurl: 启用curl库，以便zabbix可以通过curl连接web服务
[root@zabbixserver zabbix-3.4.4]# make && make install
```

- 初始化

```shell
# 1. 创建zabbix存储数据的数据库。创建名为zabbix的数据库，使用的字符集是utf8
[root@zabbixserver ~]# mysql
MariaDB [(none)]> create database zabbix default charset utf8;

# 2. 创建名为zabbix的用户，密码也是zabbix，可以对zabbix数据库拥有全部权限。zabbix@'%'格式是：zabbix@客户端地址
# 在mysql中，%表示0到多个任意字符
MariaDB [(none)]> grant all on zabbix.* to zabbix@'%' identified by 'zabbix';
# 授权zabbix用户可以在本机登陆
MariaDB [(none)]> grant all on zabbix.* to zabbix@'localhost' identified by 'zabbix';
MariaDB [(none)]> exit

# 3. 导入zabbix表结构，3个sql文件按顺序导入
[root@zabbixserver ~]# cd lnmp_soft/zabbix-3.4.4/database/mysql/
[root@zabbixserver mysql]# ls
data.sql  images.sql  schema.sql
# -u指定用户名，-p指定密码，第3个zabbix是数据库
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < schema.sql 
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < images.sql 
[root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < data.sql 

# 4. 配置zabbix_server服务
[root@zabbixserver ~]# vim /usr/local/etc/zabbix_server.conf
 12 # ListenPort=10051    # 不用改，了解端口号
 38 LogFile=/tmp/zabbix_server.log   # 不用改，日志文件位置
 85 DBHost=localhost      # 数据库服务器地址
 95 DBName=zabbix         # 不用改，数据库名
111 DBUser=zabbix         # 不用改，连接数据库的用户
119 DBPassword=zabbix     # 连接数据库的密码

# 5. 创建用于运行zabbix的用户
# -s /sbin/nologin: 用户不能登陆系统
[root@zabbixserver ~]# useradd -s /sbin/nologin zabbix

# 6. 创建用于管理zabbix的service文件
# service文件格式参考：https://www.cnblogs.com/gongxianjin/p/15673132.html
[root@zabbixserver ~]# vim /usr/lib/systemd/system/zabbix_server.service
[Unit]
Description=zabbix server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/tmp/zabbix_server.pid
ExecStart=/usr/local/sbin/zabbix_server
ExecStop=/bin/kill $MAINPID

[Install]
WantedBy=multi-user.target

# 7. 启动服务
# 新建service文件，一般需要执行systemctl daemon-reload，否则有可能识别不到
[root@zabbixserver ~]# systemctl daemon-reload 
[root@zabbixserver ~]# systemctl enable zabbix_server.service 
[root@zabbixserver ~]# systemctl start zabbix_server.service 
[root@zabbixserver ~]# ss -tlnp | grep :10051
LISTEN     0      128          *:10051

# 8. 配置agent，自己被监控
[root@zabbixserver ~]# vim /usr/local/etc/zabbix_agentd.conf
 30 LogFile=/tmp/zabbix_agentd.log   # 不用改，日志位置
 93 Server=127.0.0.1,192.168.4.5     # 指定接受哪些服务器监控
101 # ListenPort=10050               # 不用改，指定端口号
134 ServerActive=127.0.0.1,192.168.4.5  # 主动汇报信息给哪些服务器
145 Hostname=zabbixserver           # 本机的主机名
280 UnsafeUserParameters=1           # 允许用户自定义监控项

# 9. 配置agent服务的service文件
[root@zabbixserver ~]# vim /usr/lib/systemd/system/zabbix_agentd.service
[Unit]
Description=zabbix agent
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/tmp/zabbix_agentd.pid
ExecStart=/usr/local/sbin/zabbix_agentd
ExecStop=/bin/kill $MAINPID

[Install]
WantedBy=multi-user.target

# 10. 启服务
[root@zabbixserver ~]# systemctl daemon-reload
[root@zabbixserver ~]# systemctl start zabbix_agentd.service 
[root@zabbixserver ~]# systemctl enable zabbix_agentd.service 
[root@zabbixserver ~]# ss -tlnp | grep :10050
LISTEN     0      128          *:10050
```



> 附：删库重导入
>
> ```shell
> # 查看有哪些数据库
> MariaDB [(none)]> show databases;
> MariaDB [(none)]> drop database zabbix;
> MariaDB [(none)]> create database zabbix default charset utf8;
> MariaDB [(none)]> exit
> [root@zabbixserver ~]# cd lnmp_soft/zabbix-3.4.4/database/mysql/
> [root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < schema.sql 
> [root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < images.sql 
> [root@zabbixserver mysql]# mysql -uzabbix -pzabbix zabbix < data.sql 
> ```

- zabbix的管理是通过web页面进行的。通过web初始化zabbix

```shell
# 拷贝zabbix的web页面到nginx
[root@zabbixserver ~]# ls lnmp_soft/zabbix-3.4.4/frontends/
php
[root@zabbixserver ~]# cp -r lnmp_soft/zabbix-3.4.4/frontends/php/* /usr/local/nginx/html/

# nginx运行期间，调用php-fpm服务，php-fpm需要向web目录中修改文件。php-fpm的运行用户是apache，所以apache用户需要对该目录有写权限
[root@zabbixserver ~]# chown -R apache:apache /usr/local/nginx/html/

# 访问192.168.4.5/index.php，首次访问，将会自动跳转到安装页面：http://192.168.4.5/setup.php
```

![image-20211108154951884](../imgs/image-20211108154951884.png)

![image-20211108155036640](../imgs/image-20211108155036640.png)

- 根据上面红色报错，解决zabbix web依赖的内容

```shell
# 安装依赖的软件包
[root@zabbixserver ~]# yum install -y php-gd php-xml php-bcmath php-mbstring
# 修改php.ini文件
[root@zabbixserver ~]# vim /etc/php.ini
 672 post_max_size = 16M
 384 max_execution_time = 300
 394 max_input_time = 300
 878 date.timezone = Asia/Shanghai
[root@zabbixserver ~]# systemctl restart php-fpm
# 刷新web页
```

![image-20211108160054085](../imgs/image-20211108160054085.png)

![image-20211108162814600](../imgs/image-20211108162814600.png)

连接数据库的密码，也是zabbix。

![image-20211108162959430](../imgs/image-20211108162959430.png)

![image-20211108163021394](../imgs/image-20211108163021394.png)

![image-20211108163042977](../imgs/image-20211108163042977.png)

![image-20211108163107194](../imgs/image-20211108163107194.png)

默认的登陆用户是admin，密码是zabbix。

## 配置zabbix

- 修改语言

![image-20211108164724853](../imgs/image-20211108164724853.png)

![image-20211108164800435](../imgs/image-20211108164800435.png)

![image-20211108164826100](../imgs/image-20211108164826100.png)

- 启用监控自身

![image-20211108171750489](../imgs/image-20211108171750489.png)

![image-20211108171813369](../imgs/image-20211108171813369.png)

## 配置zabbix监控web1服务器

```shell
# 监控端(zabbix server)和被控端使用的软件是同一个，只是启用不同的功能
[root@zabbixserver ~]# scp lnmp_soft/zabbix-3.4.4.tar.gz 192.168.4.100:/root

# 安装编译agent需要的依赖环境
[root@web1 ~]# yum install -y gcc pcre-devel autoconf

# 编译agent
[root@web1 ~]# tar xf zabbix-3.4.4.tar.gz 
[root@web1 ~]# cd zabbix-3.4.4/
[root@web1 zabbix-3.4.4]# ./configure --enable-agent
[root@web1 zabbix-3.4.4]# make && make install

# 修改配置文件
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf
 30 LogFile=/tmp/zabbix_agentd.log   # 日志位置，不用改
 69 EnableRemoteCommands=1    # 允许监控端远程执行命令
 93 Server=127.0.0.1,192.168.4.5   # 允许自己和监控端进行数据采集
134 ServerActive=127.0.0.1,192.168.4.5  # 允许自己和监控端主动监控
145 Hostname=web1             # 自己的主机名
280 UnsafeUserParameters=1    # 允许用户自定义监控项

# 配置服务
[root@zabbixserver ~]# scp /usr/lib/systemd/system/zabbix_agentd.service 192.168.4.100:/usr/lib/systemd/system/
[root@web1 ~]# useradd -s /sbin/nologin zabbix
[root@web1 ~]# systemctl daemon-reload 
[root@web1 ~]# systemctl start zabbix_agentd.service 
[root@web1 ~]# systemctl enable zabbix_agentd.service 
[root@web1 ~]# ss -tlnp | grep :10050
LISTEN     0      128          *:10050
```

