# day02

[toc]

## 在zabbix web管理平台中配置监控

- 主机：安装了agent，被监控的主机
- 主机组：根据需求，将多台主机加入到一个主机组中，方便管理。系统默认已经创建了一些主机组。
- 模板：是监控项的集合。将模板应用到主机，主机就可以直接拥有模板中的所有监控项。系统中默认已经创建了一些模板。

### 添加被监控的主机

- 登陆zabbix：http://192.168.88.5/index.php

![image-20211009103110365](../imgs/image-20211009103110365.png)

![image-20211009103255479](../imgs/image-20211009103255479.png)

![image-20211009103513705](../imgs/image-20211009103513705.png)

- 查看监控数据

![image-20211009104234567](../imgs/image-20211009104234567.png)

需要查看哪个项目，可以点击右侧的“图形”

![image-20211009104356220](../imgs/image-20211009104356220.png)

常用的监控指标：

- CPU idle time：CPU空闲时间。不宜过低。
- Processor load(1 min average per core) / Processor load(5 min average per core) / Processor load(15 min average per core)：每核心1分钟、5分钟、15分钟的平均负载。这个值不应长期大于1。
- Free disk space on /：根分区剩余的空间
- Free disk space on /(percentage)：根分区剩余的空间百分比
- Available memory：可用内存
- Incoming network traffic on eth0：eth0网卡进入的流量
- Outgoing network traffic on eth0：eth0网卡外出的流量
- Maximum number of processes：系统最多运行的进程数
- Number of logged in users：已登陆的用户数

## 自定义监控项

### 实现监控web1用户数量的监控项

1. 在被控端创建key。被控端被监控的内容叫作key，可以理解为它就是一个变量名，具体的名字自己决定。
2. 在web页面中创建监控项。监控项对应key值。
3. 监控项存在应用集中。应用集就是相似监控项的集合。
4. 应用集存在模板中。一个模板可以包含多个应用集。

```mermaid
graph LR
templ(模板)--包含-->appset(应用集)
appset--包含-->mon(监控项)
mon--对应-->key(key)
```

#### 在被控端创建key

- 创建key的语法

```shell
UserParameter=自定义key值,命令
# 命令的执行结果，是key的value
```

- 在web1上声明自定义监控项所包含的配置文件。系统将会到`/usr/local/etc/zabbix_agentd.conf.d/`查找自定义监控项

```shell
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf
264 Include=/usr/local/etc/zabbix_agentd.conf.d/
```

- 创建自定义配置文件。文件名自定义

```shell
[root@web1 ~]# vim /usr/local/etc/zabbix_agentd.conf.d/count.line.passwd
UserParameter=count.line.passwd,sed -n '$=' /etc/passwd
```

- 验证

```shell
[root@web1 ~]# systemctl restart zabbix_agentd.service 
[root@web1 ~]# zabbix_get -s 127.0.0.1 -k count.line.passwd
[root@zabbixserver ~]# zabbix_get -s 192.168.88.100 -k count.line.passwd
# -s指定获取哪台主机的值
# -k指定key
```

#### 创建模板

创建名为count.line.passwd的模板。该模板属于名为count-passwd的组，该组不存在则自动创建。

![image-20211009114126788](../imgs/image-20211009114126788.png)

![image-20211009114246453](../imgs/image-20211009114246453.png)

#### 创建应用集

- 创建名为`count_line_passwd`的应用集

![image-20211009114531862](../imgs/image-20211009114531862.png)

![image-20211009114608482](../imgs/image-20211009114608482.png)

![image-20211009114649712](../imgs/image-20211009114649712.png)

#### 在应用集中创建监控项

- 创建名为`count_line_passwd_item`的监控项

![image-20211009114752343](../imgs/image-20211009114752343.png)

![image-20211009114834521](../imgs/image-20211009114834521.png)

![image-20211009114956605](../imgs/image-20211009114956605.png)

#### 应用模板到主机

![image-20211009115659638](../imgs/image-20211009115659638.png)

![image-20211009115738765](../imgs/image-20211009115738765.png)

#### 查看结果

- 注意，刚应用的模板，不会立即取得数据，需要等一会

![image-20211009115844587](../imgs/image-20211009115844587.png)

## 配置告警

- 默认情况下，监控项不会自动发送告警消息
- 需要配置触发器与告警，并且通过通知方式发送信息给联系人
- 触发器：设置条件，当条件达到时，将会执行某个动作
- 动作：触发器条件达到之后要采取的行为，比如发邮件或执行命令

### 用户数超35时，发送告警邮件

- 当web1的用户数超过35时，认为这是一个问题(Problem)
- 当出现问题时，将会执行动作。
- 执行的动作是给管理员发邮件。
- 给管理员发邮件，还要配置邮件服务器的地址，以及管理员的email地址

#### 实施

创建触发器规则

![image-20211009141857021](../imgs/image-20211009141857021.png)

![image-20211009141937098](../imgs/image-20211009141937098.png)

创建名为`password_line_gt_35`的触发器

![image-20211009142113266](../imgs/image-20211009142113266.png)

点击添加后，出现下图：

![image-20211009142224765](../imgs/image-20211009142224765.png)

添加表达式之后的结果如下：

![image-20211009142329729](../imgs/image-20211009142329729.png)

表达式语法：

```shell
{<server>:<key>.<function>(<parameter>)}<operator><constant>
{<主机>:<key>.<函数>(<参数>)}<操作符><常量>
```

例：

```shell
{count.line.passwd:count.line.passwd.last()}>35
# count.line.passwd是模板
# count.line.passwd是在被控端上定义的key
# last是函数，表示最近的一次取值
# > 是操作符
# 35 是常量
# 以上表达式的含义是：应用count.line.passwd模板的主机，它的count.line.passwd最近一次取值大于35，则状态为“问题”，即Problem
```

创建邮件类型的报警媒介

![image-20211009143748657](../imgs/image-20211009143748657.png)

![image-20211009144132609](../imgs/image-20211009144132609.png)

为用户关联邮箱

![image-20211009144729491](../imgs/image-20211009144729491.png)

![image-20211009144820740](../imgs/image-20211009144820740.png)



![image-20211009144955348](../imgs/image-20211009144955348.png)

创建动作：当出现Problem状态时，给admin发邮件

![image-20211009151612465](../imgs/image-20211009151612465.png)

![image-20220314141636627](../imgs/image-20220314141636627.png)

![image-20211009152241662](../imgs/image-20211009152241662.png)

点击“新的”之后，如下图：

![image-20211009152452328](../imgs/image-20211009152452328.png)

验证

```shell
# 配置zabbix服务器成为邮件服务器
[root@zabbixserver ~]# yum install -y postfix mailx
[root@zabbixserver ~]# systemctl enable postfix --now
[root@zabbixserver ~]# ss -tlnp | grep :25
LISTEN     0      100    127.0.0.1:25

# 在web1上创建用户
[root@web1 ~]# for user in user{1..20}
> do
> useradd $user
> done
[root@web1 ~]# zabbix_get -s 127.0.0.1 -k count.line.passwd
43
```

![image-20211009154251273](../imgs/image-20211009154251273.png)

![image-20211009154404010](../imgs/image-20211009154404010.png)

![image-20211009154512197](../imgs/image-20211009154512197.png)

```shell
# 在zabbixserver上查看邮件
[root@zabbixserver ~]# mail
>N  1 zabbix@localhost.loc  Tue Nov  9 14:48  21/941   
# N表示未读，1是邮件编号
# 回车默认查看最新一封邮件
& q      # 退出
```

## 自动发现

- 当被监控的设备非常多的时候，手工添加将会变得非常不方便
- 可以使用自动发现功能，实现添加主机、添加到主机组、 链接模板

- 自动发现流程：
  - 创建自动发现规则
  - 创建动作，当主机被发现之后，执行什么操作
  - 通过动作，添加主机，将模板应用到发现的主机

#### 配置自动发现

- 创建自动发现规则

![image-20211009163820665](../imgs/image-20211009163820665.png)

![image-20211009164038717](../imgs/image-20211009164038717.png)

- 创建动作

![image-20211009164529777](../imgs/image-20211009164529777.png)

![image-20211009164634515](../imgs/image-20211009164634515.png)

![image-20211009164738033](../imgs/image-20211009164738033.png)

![image-20211009164857584](../imgs/image-20211009164857584.png)

![image-20211109170417346](../imgs/image-20211109170417346.png)

在zabbix web页面中查看web2是否已被发现

![image-20211009170022854](../imgs/image-20211009170022854.png)

web2是通过ssh发现的，它上面还没有配置zabbix agent，所以即使发现了，也无法监控。需要在web2上配置agent。

```shell
[root@web1 ~]# scp zabbix-3.4.4.tar.gz 192.168.88.200:/root/

[root@web2 ~]# yum install -y gcc pcre-devel autoconf
[root@web2 ~]# tar xf zabbix-3.4.4.tar.gz 
[root@web2 ~]# cd zabbix-3.4.4/
[root@web2 zabbix-3.4.4]# ./configure --enable-agent && make && make install
[root@web2 zabbix-3.4.4]# useradd -s /sbin/nologin zabbix
[root@web2 ~]# vim /usr/local/etc/zabbix_agentd.conf
 69 EnableRemoteCommands=1
 93 Server=127.0.0.1,192.168.88.5
134 ServerActive=127.0.0.1,192.168.88.5
145 Hostname=web2
280 UnsafeUserParameters=1
[root@zabbixserver ~]# scp /usr/lib/systemd/system/zabbix_agentd.service 192.168.88.200:/usr/lib/systemd/system/
[root@web2 ~]# systemctl daemon-reload
[root@web2 ~]# systemctl enable zabbix_agentd.service --now
[root@web2 ~]# ss -tlnp | grep :10050
LISTEN     0      128          *:10050
```

### 实施主动监控

- 默认zabbix使用的是被动监控，主被动监控都是针对被监控主机而言的。
- 被动监控：Server向Agent发起请求，索取监控数据。此种模式常用
- 主动监控：Agent向Server发起连接，向Server汇报

#### 配置web2使用主动监控

- 修改配置文件，只使用主动监控

```shell
[root@web2 ~]# vim /usr/local/etc/zabbix_agentd.conf
 93 # Server=127.0.0.1,192.168.88.5   # 因为采用主动监控，所以不接受其他主机的查询
118 StartAgents=0   # 不接受被动检查，也不开启任何端口
134 ServerActive=192.168.88.5   # 只向server汇报
183 RefreshActiveChecks=120    # 120秒检查一次配置
264 Include=/usr/local/etc/zabbix_agentd.conf.d/
280 UnsafeUserParameters=1

# 重启服务
[root@web2 ~]# systemctl restart zabbix_agentd.service 
[root@web2 ~]# ss -tlnp | grep :10050    # 端口号消失
```

创建用于主动监控的模板。可以基于现有的模板，克隆之后修改。

1. 克隆现有模板

![image-20211009174454344](../imgs/image-20211009174454344.png)

![image-20211009174527405](../imgs/image-20211009174527405.png)

![image-20211009174642418](../imgs/image-20211009174642418.png)

![image-20211009174721180](../imgs/image-20211009174721180.png)

2. 修改克隆的模板，将监控项改为主动监控

![image-20211009175034682](../imgs/image-20211009175034682.png)

![image-20211009175624108](../imgs/image-20211009175624108.png)

![image-20211009175143879](../imgs/image-20211009175143879.png)

![image-20211009175207955](../imgs/image-20211009175207955.png)

![image-20211009175256277](../imgs/image-20211009175256277.png)

![image-20211009175314253](../imgs/image-20211009175314253.png)

克隆的模板有3项不支持主动，把它们禁用。

![image-20211009175426736](../imgs/image-20211009175426736.png)

添加使用主动监控的主机

![image-20211009175936684](../imgs/image-20211009175936684.png)

下图中，注意主机名应该与被监控的主机的名字相同

![image-20211009180109687](../imgs/image-20211009180109687.png)

![image-20211009180207488](../imgs/image-20211009180207488.png)

查看最新数据

![image-20211009180511569](../imgs/image-20211009180511569.png)

## 拓扑图和聚合图形

### 拓扑图

- 在zabbix中可以创建拓扑图，反映拓扑结构

![image-20211011093108735](../imgs/image-20211011093108735.png)

![image-20211011093138268](../imgs/image-20211011093138268.png)

添加元素：

- 图标：可以是一个装饰图形，也可以对应具体的设备
- 形状：长方形、圆形
- 链接：连线，多个设备之间才能使用链接

#### 完善拓扑图

- 添加交换机图标

![image-20211011093625759](../imgs/image-20211011093625759.png)

![image-20211011093738831](../imgs/image-20211011093738831.png)

- 添加链接：圈中两台设备，点链接边上的添加

![image-20211011093924690](../imgs/image-20211011093924690.png)

- 添加2台web服务器

![image-20211011094401267](../imgs/image-20211011094401267.png)

![image-20211011094508613](../imgs/image-20211011094508613.png)

### 聚合图形

- 将常用的页面放到一个统一页面中查看

![image-20211011101231497](../imgs/image-20211011101231497.png)

![image-20211011101321401](../imgs/image-20211011101321401.png)

![image-20211011101405428](../imgs/image-20211011101405428.png)

![image-20211011101429254](../imgs/image-20211011101429254.png)

![image-20211011101525333](../imgs/image-20211011101525333.png)

点击更改之后，添加新望出现在该单元格的数据。

![image-20211011101621625](../imgs/image-20211011101621625.png)

![image-20211011101657170](../imgs/image-20211011101657170.png)

使用同样的方法，点击其他的“更改”

![image-20211011101923352](../imgs/image-20211011101923352.png)

《钉钉机器人告警》https://www.jianshu.com/p/6317121da5a4

